<?php
/*
File Name       : LoginController.php
Created Date    : 26/10/2016
*/
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Entity\Pagu;
use AppBundle\Entity\Jurusan;
// use AppBundle\Repository\UserRepository;


class PaguController extends Controller
{
    /**
     * @Route("/pagu", name="pagu")
     */
    public function list(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $session = new Session();
        //Insert Action
        if($request->isMethod('POST')){
            $query = $em->createQuery(
            'SELECT t
            FROM AppBundle:TahunAnggaran t
            WHERE t.tahunAnggaran=:tahun'
            )->setParameter('tahun',$request->request->get('tahun'));
            $ta = $query->getOneOrNullResult();
            

            $pagu = new Pagu();
            $jurusan = $em->getRepository('AppBundle:Jurusan')->findOneBy(array('idJurusan'=>$session->get('jurusan')->getIdJurusan()));
            // echo '<pre>';
            // \Doctrine\Common\Util\Debug::dump($jurusan);
            // echo '</pre>';
            // exit;
            $pagu->setIdJurusan($jurusan);
            $pagu->setTahunAnggaran($ta);
            $pagu->setPaguAlat($request->request->get('pagu_alat'));
            $pagu->setTanggal(date_create(date('Y-m-d H:m:s')));
            // $em->merge($session->get('jurusan'));
            $em->persist($pagu);
            $em->flush();
        }
        
        // echo $session->get('jurusan')->getIdJurusan();
        // exit;
        
        $query = $em->createQuery(
            'SELECT p
            FROM AppBundle:Pagu p
            WHERE p.idJurusan=:jurusan'
            )->setParameter('jurusan',$session->get('jurusan')->getIdJurusan());

        $pagu = $query->getResult();

        $query = $em->createQuery(
            'SELECT SUM(p.paguAlat) as total_pagu
            FROM AppBundle:Pagu p
            WHERE p.idJurusan=:jurusan'
            )->setParameter('jurusan',$session->get('jurusan')->getIdJurusan());

        $total = $query->getOneOrNullResult();
            
        // echo '<pre>';
        // \Doctrine\Common\Util\Debug::dump($pagu);
        // echo '</pre>';
        // exit;
        return $this->render('pagu/list.html.twig',[
            'page'=>"pagu",
            'data'=>$pagu,
            'total'=>$total
            ]);
    }
    /**
     * @Route(
     *      "/pagu/delete/{id}",
     *      name="pagu_delete"
     * )
     */
    public function delete($id){
        $pagu = $this->getDoctrine()
        ->getRepository('AppBundle:Pagu')
        ->find($id);
        // echo $id;
        // echo '<pre>';
        // \Doctrine\Common\Util\Debug::dump($pagu);
        // echo '</pre>';
        // exit;
        $em = $this->getDoctrine()->getManager();
        $em->remove($pagu);
        $em->flush();
        return $this->redirect('/pagu');
    }

    /**
     * @Route(
     *      "/pagu/edit",
     *      name="pagu_edit"
     * )
     */
    public function edit(Request $request){
        $id = $request->request->get('id_pagu');
        $pagu_alat = $request->request->get('pagu_alat');
        $em = $this->getDoctrine()->getManager();
        $pagu = $em->getRepository('AppBundle:Pagu')->find($id);

        if (!$pagu) {
            throw $this->createNotFoundException(
                'No Pagu found for id '.$id
            );
        }

        $pagu->setPaguAlat($pagu_alat);
        $em->flush();

        return $this->redirectToRoute('pagu');
    }
}
