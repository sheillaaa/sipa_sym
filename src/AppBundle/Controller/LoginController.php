<?php
/*
File Name       : LoginController.php
Created Date    : 26/10/2016
*/
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Entity\User;
// use AppBundle\Repository\UserRepository;


class LoginController extends Controller
{
    /**
     * @Route("/", name="login")
     */
    public function login(Request $request)
    {
        //Login Action
        if($request->isMethod('POST')){
            $session = new Session();
            $user = new User();
            $user->setUsername($request->request->get('username'));
            $user->setPassword($request->request->get('password'));
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery(
                'SELECT u,j
                FROM AppBundle:User u
                JOIN u.idJurusan j
                WHERE u.username = :username AND u.password = :password '
                )->setParameters(array('username'=>$request->request->get('username'),
                    'password'=>md5($request->request->get('password'))));

                $user = $query->getOneOrNullResult();
                // echo '<pre>';
                // \Doctrine\Common\Util\Debug::dump($user);
                // echo '</pre>';
                // exit;
                if($user){
                    $session->set('username', $request->request->get('username'));
                    $session->set('jurusan', $user->getIdJurusan());
                    return $this->redirect('/pagu');
                }else{

                // set flash messages
                    $session->getFlashBag()->add('status', 'Login Failed');
                }
            }

            return $this->render('login/form.html.twig');
        }
    /**
     * @Route("/logout", name="logout")
     */
    public function logout(Request $request)
    {
        $session = new Session();
        $session->clear();
        return $this->redirect('/');
    }
}
