<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProgressPaket
 *
 * @ORM\Table(name="progress_paket", indexes={@ORM\Index(name="FK_RELATIONSHIP_12", columns={"ID_FASE"}), @ORM\Index(name="FK_RELATIONSHIP_13", columns={"ID_USER"}), @ORM\Index(name="FK_RELATIONSHIP_14", columns={"ID_PAKET"}), @ORM\Index(name="FK_RELATIONSHIP_28", columns={"ID_USULAN"}), @ORM\Index(name="FK_RELATIONSHIP_38", columns={"ID_JENIS_USER"})})
 * @ORM\Entity
 */
class ProgressPaket
{
    /**
     * @var string
     *
     * @ORM\Column(name="STATUS", type="decimal", precision=8, scale=0, nullable=true)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="TANGGAL", type="datetime", nullable=true)
     */
    private $tanggal;

    /**
     * @var string
     *
     * @ORM\Column(name="REVISI_KE", type="decimal", precision=8, scale=0, nullable=true)
     */
    private $revisiKe;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_PROGRESS_PAKET", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idProgressPaket;

    /**
     * @var \AppBundle\Entity\Fase
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Fase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_FASE", referencedColumnName="ID_FASE")
     * })
     */
    private $idFase;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_USER", referencedColumnName="ID_USER")
     * })
     */
    private $idUser;

    /**
     * @var \AppBundle\Entity\Paket
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Paket")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_PAKET", referencedColumnName="ID_PAKET")
     * })
     */
    private $idPaket;

    /**
     * @var \AppBundle\Entity\Usulan
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Usulan")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_USULAN", referencedColumnName="ID_USULAN")
     * })
     */
    private $idUsulan;

    /**
     * @var \AppBundle\Entity\JenisUser
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\JenisUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_JENIS_USER", referencedColumnName="ID_JENIS_USER")
     * })
     */
    private $idJenisUser;



    /**
     * Set status
     *
     * @param string $status
     *
     * @return ProgressPaket
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set tanggal
     *
     * @param \DateTime $tanggal
     *
     * @return ProgressPaket
     */
    public function setTanggal($tanggal)
    {
        $this->tanggal = $tanggal;

        return $this;
    }

    /**
     * Get tanggal
     *
     * @return \DateTime
     */
    public function getTanggal()
    {
        return $this->tanggal;
    }

    /**
     * Set revisiKe
     *
     * @param string $revisiKe
     *
     * @return ProgressPaket
     */
    public function setRevisiKe($revisiKe)
    {
        $this->revisiKe = $revisiKe;

        return $this;
    }

    /**
     * Get revisiKe
     *
     * @return string
     */
    public function getRevisiKe()
    {
        return $this->revisiKe;
    }

    /**
     * Get idProgressPaket
     *
     * @return integer
     */
    public function getIdProgressPaket()
    {
        return $this->idProgressPaket;
    }

    /**
     * Set idFase
     *
     * @param \AppBundle\Entity\Fase $idFase
     *
     * @return ProgressPaket
     */
    public function setIdFase(\AppBundle\Entity\Fase $idFase = null)
    {
        $this->idFase = $idFase;

        return $this;
    }

    /**
     * Get idFase
     *
     * @return \AppBundle\Entity\Fase
     */
    public function getIdFase()
    {
        return $this->idFase;
    }

    /**
     * Set idUser
     *
     * @param \AppBundle\Entity\User $idUser
     *
     * @return ProgressPaket
     */
    public function setIdUser(\AppBundle\Entity\User $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set idPaket
     *
     * @param \AppBundle\Entity\Paket $idPaket
     *
     * @return ProgressPaket
     */
    public function setIdPaket(\AppBundle\Entity\Paket $idPaket = null)
    {
        $this->idPaket = $idPaket;

        return $this;
    }

    /**
     * Get idPaket
     *
     * @return \AppBundle\Entity\Paket
     */
    public function getIdPaket()
    {
        return $this->idPaket;
    }

    /**
     * Set idUsulan
     *
     * @param \AppBundle\Entity\Usulan $idUsulan
     *
     * @return ProgressPaket
     */
    public function setIdUsulan(\AppBundle\Entity\Usulan $idUsulan = null)
    {
        $this->idUsulan = $idUsulan;

        return $this;
    }

    /**
     * Get idUsulan
     *
     * @return \AppBundle\Entity\Usulan
     */
    public function getIdUsulan()
    {
        return $this->idUsulan;
    }

    /**
     * Set idJenisUser
     *
     * @param \AppBundle\Entity\JenisUser $idJenisUser
     *
     * @return ProgressPaket
     */
    public function setIdJenisUser(\AppBundle\Entity\JenisUser $idJenisUser = null)
    {
        $this->idJenisUser = $idJenisUser;

        return $this;
    }

    /**
     * Get idJenisUser
     *
     * @return \AppBundle\Entity\JenisUser
     */
    public function getIdJenisUser()
    {
        return $this->idJenisUser;
    }
}
