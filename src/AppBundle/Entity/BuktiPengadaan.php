<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BuktiPengadaan
 *
 * @ORM\Table(name="bukti_pengadaan", indexes={@ORM\Index(name="FK_RELATIONSHIP_35", columns={"ID_PAKET"})})
 * @ORM\Entity
 */
class BuktiPengadaan
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="TANGGAL", type="datetime", nullable=true)
     */
    private $tanggal;

    /**
     * @var string
     *
     * @ORM\Column(name="FILE", type="text", length=65535, nullable=true)
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(name="KETERANGAN", type="text", length=65535, nullable=true)
     */
    private $keterangan;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_BUKTI", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idBukti;

    /**
     * @var \AppBundle\Entity\Paket
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Paket")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_PAKET", referencedColumnName="ID_PAKET")
     * })
     */
    private $idPaket;



    /**
     * Set tanggal
     *
     * @param \DateTime $tanggal
     *
     * @return BuktiPengadaan
     */
    public function setTanggal($tanggal)
    {
        $this->tanggal = $tanggal;

        return $this;
    }

    /**
     * Get tanggal
     *
     * @return \DateTime
     */
    public function getTanggal()
    {
        return $this->tanggal;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     * @return BuktiPengadaan
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set keterangan
     *
     * @param string $keterangan
     *
     * @return BuktiPengadaan
     */
    public function setKeterangan($keterangan)
    {
        $this->keterangan = $keterangan;

        return $this;
    }

    /**
     * Get keterangan
     *
     * @return string
     */
    public function getKeterangan()
    {
        return $this->keterangan;
    }

    /**
     * Get idBukti
     *
     * @return integer
     */
    public function getIdBukti()
    {
        return $this->idBukti;
    }

    /**
     * Set idPaket
     *
     * @param \AppBundle\Entity\Paket $idPaket
     *
     * @return BuktiPengadaan
     */
    public function setIdPaket(\AppBundle\Entity\Paket $idPaket = null)
    {
        $this->idPaket = $idPaket;

        return $this;
    }

    /**
     * Get idPaket
     *
     * @return \AppBundle\Entity\Paket
     */
    public function getIdPaket()
    {
        return $this->idPaket;
    }
}
