<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reminder
 *
 * @ORM\Table(name="reminder", indexes={@ORM\Index(name="FK_RELATIONSHIP_43", columns={"ID_FASE"}), @ORM\Index(name="FK_RELATIONSHIP_44", columns={"ID_USER"})})
 * @ORM\Entity
 */
class Reminder
{
    /**
     * @var string
     *
     * @ORM\Column(name="STATUS", type="decimal", precision=8, scale=0, nullable=true)
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_REMINDER", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idReminder;

    /**
     * @var \AppBundle\Entity\Fase
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Fase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_FASE", referencedColumnName="ID_FASE")
     * })
     */
    private $idFase;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_USER", referencedColumnName="ID_USER")
     * })
     */
    private $idUser;



    /**
     * Set status
     *
     * @param string $status
     *
     * @return Reminder
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get idReminder
     *
     * @return integer
     */
    public function getIdReminder()
    {
        return $this->idReminder;
    }

    /**
     * Set idFase
     *
     * @param \AppBundle\Entity\Fase $idFase
     *
     * @return Reminder
     */
    public function setIdFase(\AppBundle\Entity\Fase $idFase = null)
    {
        $this->idFase = $idFase;

        return $this;
    }

    /**
     * Get idFase
     *
     * @return \AppBundle\Entity\Fase
     */
    public function getIdFase()
    {
        return $this->idFase;
    }

    /**
     * Set idUser
     *
     * @param \AppBundle\Entity\User $idUser
     *
     * @return Reminder
     */
    public function setIdUser(\AppBundle\Entity\User $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdUser()
    {
        return $this->idUser;
    }
}
