<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Lokasi
 *
 * @ORM\Table(name="lokasi", indexes={@ORM\Index(name="FK_RELATIONSHIP_15", columns={"ID_JURUSAN"})})
 * @ORM\Entity
 */
class Lokasi
{
    /**
     * @var string
     *
     * @ORM\Column(name="NAMA_LOKASI", type="string", length=50, nullable=true)
     */
    private $namaLokasi;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_LOKASI", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idLokasi;

    /**
     * @var \AppBundle\Entity\Jurusan
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Jurusan")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_JURUSAN", referencedColumnName="ID_JURUSAN")
     * })
     */
    private $idJurusan;



    /**
     * Set namaLokasi
     *
     * @param string $namaLokasi
     *
     * @return Lokasi
     */
    public function setNamaLokasi($namaLokasi)
    {
        $this->namaLokasi = $namaLokasi;

        return $this;
    }

    /**
     * Get namaLokasi
     *
     * @return string
     */
    public function getNamaLokasi()
    {
        return $this->namaLokasi;
    }

    /**
     * Get idLokasi
     *
     * @return integer
     */
    public function getIdLokasi()
    {
        return $this->idLokasi;
    }

    /**
     * Set idJurusan
     *
     * @param \AppBundle\Entity\Jurusan $idJurusan
     *
     * @return Lokasi
     */
    public function setIdJurusan(\AppBundle\Entity\Jurusan $idJurusan = null)
    {
        $this->idJurusan = $idJurusan;

        return $this;
    }

    /**
     * Get idJurusan
     *
     * @return \AppBundle\Entity\Jurusan
     */
    public function getIdJurusan()
    {
        return $this->idJurusan;
    }
}
