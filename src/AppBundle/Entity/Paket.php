<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Paket
 *
 * @ORM\Table(name="paket", indexes={@ORM\Index(name="FK_REFERENCE_40", columns={"TAHUN_ANGGARAN"}), @ORM\Index(name="FK_RELATIONSHIP_2", columns={"ID_TIM_HPS"}), @ORM\Index(name="FK_RELATIONSHIP_4", columns={"ID_USER"})})
 * @ORM\Entity
 */
class Paket
{
    /**
     * @var string
     *
     * @ORM\Column(name="NAMA_PAKET", type="text", length=65535, nullable=true)
     */
    private $namaPaket;

    /**
     * @var string
     *
     * @ORM\Column(name="STATUS", type="decimal", precision=8, scale=0, nullable=true)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="TANGGAL_DIBUAT", type="datetime", nullable=true)
     */
    private $tanggalDibuat;

    /**
     * @var integer
     *
     * @ORM\Column(name="TOTAL_ANGGARAN", type="bigint", nullable=true)
     */
    private $totalAnggaran;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="LAST_UPDATE", type="datetime", nullable=true)
     */
    private $lastUpdate;

    /**
     * @var string
     *
     * @ORM\Column(name="KETERANGAN_GAGAL_LELANG", type="text", length=65535, nullable=true)
     */
    private $keteranganGagalLelang;

    /**
     * @var string
     *
     * @ORM\Column(name="PENYEDIA", type="string", length=50, nullable=true)
     */
    private $penyedia;

    /**
     * @var integer
     *
     * @ORM\Column(name="WAKTU_PENGADAAN", type="integer", nullable=true)
     */
    private $waktuPengadaan;

    /**
     * @var string
     *
     * @ORM\Column(name="TYPE_WAKTU", type="string", length=20, nullable=true)
     */
    private $typeWaktu;

    /**
     * @var boolean
     *
     * @ORM\Column(name="STATUS_BAYAR", type="boolean", nullable=true)
     */
    private $statusBayar;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_PAKET", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPaket;

    /**
     * @var \AppBundle\Entity\TahunAnggaran
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TahunAnggaran")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="TAHUN_ANGGARAN", referencedColumnName="TAHUN_ANGGARAN")
     * })
     */
    private $tahunAnggaran;

    /**
     * @var \AppBundle\Entity\TimHps
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TimHps")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_TIM_HPS", referencedColumnName="ID_TIM_HPS")
     * })
     */
    private $idTimHps;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_USER", referencedColumnName="ID_USER")
     * })
     */
    private $idUser;



    /**
     * Set namaPaket
     *
     * @param string $namaPaket
     *
     * @return Paket
     */
    public function setNamaPaket($namaPaket)
    {
        $this->namaPaket = $namaPaket;

        return $this;
    }

    /**
     * Get namaPaket
     *
     * @return string
     */
    public function getNamaPaket()
    {
        return $this->namaPaket;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Paket
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set tanggalDibuat
     *
     * @param \DateTime $tanggalDibuat
     *
     * @return Paket
     */
    public function setTanggalDibuat($tanggalDibuat)
    {
        $this->tanggalDibuat = $tanggalDibuat;

        return $this;
    }

    /**
     * Get tanggalDibuat
     *
     * @return \DateTime
     */
    public function getTanggalDibuat()
    {
        return $this->tanggalDibuat;
    }

    /**
     * Set totalAnggaran
     *
     * @param integer $totalAnggaran
     *
     * @return Paket
     */
    public function setTotalAnggaran($totalAnggaran)
    {
        $this->totalAnggaran = $totalAnggaran;

        return $this;
    }

    /**
     * Get totalAnggaran
     *
     * @return integer
     */
    public function getTotalAnggaran()
    {
        return $this->totalAnggaran;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return Paket
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Set keteranganGagalLelang
     *
     * @param string $keteranganGagalLelang
     *
     * @return Paket
     */
    public function setKeteranganGagalLelang($keteranganGagalLelang)
    {
        $this->keteranganGagalLelang = $keteranganGagalLelang;

        return $this;
    }

    /**
     * Get keteranganGagalLelang
     *
     * @return string
     */
    public function getKeteranganGagalLelang()
    {
        return $this->keteranganGagalLelang;
    }

    /**
     * Set penyedia
     *
     * @param string $penyedia
     *
     * @return Paket
     */
    public function setPenyedia($penyedia)
    {
        $this->penyedia = $penyedia;

        return $this;
    }

    /**
     * Get penyedia
     *
     * @return string
     */
    public function getPenyedia()
    {
        return $this->penyedia;
    }

    /**
     * Set waktuPengadaan
     *
     * @param integer $waktuPengadaan
     *
     * @return Paket
     */
    public function setWaktuPengadaan($waktuPengadaan)
    {
        $this->waktuPengadaan = $waktuPengadaan;

        return $this;
    }

    /**
     * Get waktuPengadaan
     *
     * @return integer
     */
    public function getWaktuPengadaan()
    {
        return $this->waktuPengadaan;
    }

    /**
     * Set typeWaktu
     *
     * @param string $typeWaktu
     *
     * @return Paket
     */
    public function setTypeWaktu($typeWaktu)
    {
        $this->typeWaktu = $typeWaktu;

        return $this;
    }

    /**
     * Get typeWaktu
     *
     * @return string
     */
    public function getTypeWaktu()
    {
        return $this->typeWaktu;
    }

    /**
     * Set statusBayar
     *
     * @param boolean $statusBayar
     *
     * @return Paket
     */
    public function setStatusBayar($statusBayar)
    {
        $this->statusBayar = $statusBayar;

        return $this;
    }

    /**
     * Get statusBayar
     *
     * @return boolean
     */
    public function getStatusBayar()
    {
        return $this->statusBayar;
    }

    /**
     * Get idPaket
     *
     * @return integer
     */
    public function getIdPaket()
    {
        return $this->idPaket;
    }

    /**
     * Set tahunAnggaran
     *
     * @param \AppBundle\Entity\TahunAnggaran $tahunAnggaran
     *
     * @return Paket
     */
    public function setTahunAnggaran(\AppBundle\Entity\TahunAnggaran $tahunAnggaran = null)
    {
        $this->tahunAnggaran = $tahunAnggaran;

        return $this;
    }

    /**
     * Get tahunAnggaran
     *
     * @return \AppBundle\Entity\TahunAnggaran
     */
    public function getTahunAnggaran()
    {
        return $this->tahunAnggaran;
    }

    /**
     * Set idTimHps
     *
     * @param \AppBundle\Entity\TimHps $idTimHps
     *
     * @return Paket
     */
    public function setIdTimHps(\AppBundle\Entity\TimHps $idTimHps = null)
    {
        $this->idTimHps = $idTimHps;

        return $this;
    }

    /**
     * Get idTimHps
     *
     * @return \AppBundle\Entity\TimHps
     */
    public function getIdTimHps()
    {
        return $this->idTimHps;
    }

    /**
     * Set idUser
     *
     * @param \AppBundle\Entity\User $idUser
     *
     * @return Paket
     */
    public function setIdUser(\AppBundle\Entity\User $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdUser()
    {
        return $this->idUser;
    }
}
