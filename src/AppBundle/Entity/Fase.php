<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fase
 *
 * @ORM\Table(name="fase")
 * @ORM\Entity
 */
class Fase
{
    /**
     * @var string
     *
     * @ORM\Column(name="NAMA_FASE", type="string", length=50, nullable=true)
     */
    private $namaFase;

    /**
     * @var integer
     *
     * @ORM\Column(name="WAKTU_PELAKSANAAN", type="integer", nullable=false)
     */
    private $waktuPelaksanaan;

    /**
     * @var integer
     *
     * @ORM\Column(name="WAKTU_TAMBAHAN", type="integer", nullable=false)
     */
    private $waktuTambahan;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_FASE", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idFase;



    /**
     * Set namaFase
     *
     * @param string $namaFase
     *
     * @return Fase
     */
    public function setNamaFase($namaFase)
    {
        $this->namaFase = $namaFase;

        return $this;
    }

    /**
     * Get namaFase
     *
     * @return string
     */
    public function getNamaFase()
    {
        return $this->namaFase;
    }

    /**
     * Set waktuPelaksanaan
     *
     * @param integer $waktuPelaksanaan
     *
     * @return Fase
     */
    public function setWaktuPelaksanaan($waktuPelaksanaan)
    {
        $this->waktuPelaksanaan = $waktuPelaksanaan;

        return $this;
    }

    /**
     * Get waktuPelaksanaan
     *
     * @return integer
     */
    public function getWaktuPelaksanaan()
    {
        return $this->waktuPelaksanaan;
    }

    /**
     * Set waktuTambahan
     *
     * @param integer $waktuTambahan
     *
     * @return Fase
     */
    public function setWaktuTambahan($waktuTambahan)
    {
        $this->waktuTambahan = $waktuTambahan;

        return $this;
    }

    /**
     * Get waktuTambahan
     *
     * @return integer
     */
    public function getWaktuTambahan()
    {
        return $this->waktuTambahan;
    }

    /**
     * Get idFase
     *
     * @return integer
     */
    public function getIdFase()
    {
        return $this->idFase;
    }
}
