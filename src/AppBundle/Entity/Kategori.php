<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Kategori
 *
 * @ORM\Table(name="kategori")
 * @ORM\Entity
 */
class Kategori
{
    /**
     * @var string
     *
     * @ORM\Column(name="KATEGORI", type="string", length=50, nullable=true)
     */
    private $kategori;

    /**
     * @var string
     *
     * @ORM\Column(name="KETERANGAN", type="text", length=65535, nullable=true)
     */
    private $keterangan;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_KATEGORI", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idKategori;



    /**
     * Set kategori
     *
     * @param string $kategori
     *
     * @return Kategori
     */
    public function setKategori($kategori)
    {
        $this->kategori = $kategori;

        return $this;
    }

    /**
     * Get kategori
     *
     * @return string
     */
    public function getKategori()
    {
        return $this->kategori;
    }

    /**
     * Set keterangan
     *
     * @param string $keterangan
     *
     * @return Kategori
     */
    public function setKeterangan($keterangan)
    {
        $this->keterangan = $keterangan;

        return $this;
    }

    /**
     * Get keterangan
     *
     * @return string
     */
    public function getKeterangan()
    {
        return $this->keterangan;
    }

    /**
     * Get idKategori
     *
     * @return integer
     */
    public function getIdKategori()
    {
        return $this->idKategori;
    }
}
