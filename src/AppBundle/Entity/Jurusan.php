<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Jurusan
 *
 * @ORM\Table(name="jurusan")
 * @ORM\Entity
 */
class Jurusan
{
    /**
     * @var string
     *
     * @ORM\Column(name="NAMA_JURUSAN", type="string", length=50, nullable=true)
     */
    private $namaJurusan;

    /**
     * @var string
     *
     * @ORM\Column(name="INISIAL", type="string", length=10, nullable=true)
     */
    private $inisial;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_JURUSAN", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idJurusan;



    /**
     * Set namaJurusan
     *
     * @param string $namaJurusan
     *
     * @return Jurusan
     */
    public function setNamaJurusan($namaJurusan)
    {
        $this->namaJurusan = $namaJurusan;

        return $this;
    }

    /**
     * Get namaJurusan
     *
     * @return string
     */
    public function getNamaJurusan()
    {
        return $this->namaJurusan;
    }

    /**
     * Set inisial
     *
     * @param string $inisial
     *
     * @return Jurusan
     */
    public function setInisial($inisial)
    {
        $this->inisial = $inisial;

        return $this;
    }

    /**
     * Get inisial
     *
     * @return string
     */
    public function getInisial()
    {
        return $this->inisial;
    }

    /**
     * Get idJurusan
     *
     * @return integer
     */
    public function getIdJurusan()
    {
        return $this->idJurusan;
    }
}
