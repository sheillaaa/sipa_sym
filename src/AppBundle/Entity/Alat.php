<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Alat
 *
 * @ORM\Table(name="alat", indexes={@ORM\Index(name="FK_RELATIONSHIP_10", columns={"ID_LOKASI"}), @ORM\Index(name="FK_RELATIONSHIP_16", columns={"ID_FASE"}), @ORM\Index(name="FK_RELATIONSHIP_18", columns={"ID_JURUSAN"}), @ORM\Index(name="FK_RELATIONSHIP_26", columns={"ID_PAKET"}), @ORM\Index(name="FK_RELATIONSHIP_29", columns={"ID_USULAN"}), @ORM\Index(name="FK_RELATIONSHIP_36", columns={"ID_KATEGORI"}), @ORM\Index(name="FK_RELATIONSHIP_6", columns={"ID_USER"})})
 * @ORM\Entity
 */
class Alat
{
    /**
     * @var string
     *
     * @ORM\Column(name="NAMA_ALAT", type="string", length=50, nullable=true)
     */
    private $namaAlat;

    /**
     * @var string
     *
     * @ORM\Column(name="SPESIFIKASI", type="text", length=65535, nullable=true)
     */
    private $spesifikasi;

    /**
     * @var string
     *
     * @ORM\Column(name="SETARA", type="text", length=65535, nullable=true)
     */
    private $setara;

    /**
     * @var string
     *
     * @ORM\Column(name="SATUAN", type="string", length=20, nullable=true)
     */
    private $satuan;

    /**
     * @var string
     *
     * @ORM\Column(name="JUMLAH_ALAT", type="decimal", precision=8, scale=0, nullable=true)
     */
    private $jumlahAlat;

    /**
     * @var string
     *
     * @ORM\Column(name="HARGA_SATUAN", type="decimal", precision=8, scale=0, nullable=true)
     */
    private $hargaSatuan;

    /**
     * @var string
     *
     * @ORM\Column(name="JUMLAH_DISTRIBUSI", type="decimal", precision=8, scale=0, nullable=true)
     */
    private $jumlahDistribusi;

    /**
     * @var string
     *
     * @ORM\Column(name="KONFIRMASI", type="text", length=65535, nullable=true)
     */
    private $konfirmasi;

    /**
     * @var string
     *
     * @ORM\Column(name="REFERENSI_TERKAIT", type="text", length=65535, nullable=true)
     */
    private $referensiTerkait;

    /**
     * @var boolean
     *
     * @ORM\Column(name="DATA_AHLI", type="boolean", nullable=true)
     */
    private $dataAhli;

    /**
     * @var string
     *
     * @ORM\Column(name="REVISI", type="decimal", precision=8, scale=0, nullable=true)
     */
    private $revisi;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="TANGGAL_UPDATE", type="datetime", nullable=true)
     */
    private $tanggalUpdate;

    /**
     * @var string
     *
     * @ORM\Column(name="NO_INVENTARIS", type="decimal", precision=8, scale=0, nullable=true)
     */
    private $noInventaris;

    /**
     * @var boolean
     *
     * @ORM\Column(name="IS_FINAL", type="boolean", nullable=true)
     */
    private $isFinal;

    /**
     * @var string
     *
     * @ORM\Column(name="REVISI_PAKET", type="decimal", precision=8, scale=0, nullable=true)
     */
    private $revisiPaket;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_ALAT", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idAlat;

    /**
     * @var \AppBundle\Entity\Lokasi
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Lokasi")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_LOKASI", referencedColumnName="ID_LOKASI")
     * })
     */
    private $idLokasi;

    /**
     * @var \AppBundle\Entity\Fase
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Fase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_FASE", referencedColumnName="ID_FASE")
     * })
     */
    private $idFase;

    /**
     * @var \AppBundle\Entity\Jurusan
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Jurusan")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_JURUSAN", referencedColumnName="ID_JURUSAN")
     * })
     */
    private $idJurusan;

    /**
     * @var \AppBundle\Entity\Paket
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Paket")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_PAKET", referencedColumnName="ID_PAKET")
     * })
     */
    private $idPaket;

    /**
     * @var \AppBundle\Entity\Usulan
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Usulan")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_USULAN", referencedColumnName="ID_USULAN")
     * })
     */
    private $idUsulan;

    /**
     * @var \AppBundle\Entity\Kategori
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Kategori")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_KATEGORI", referencedColumnName="ID_KATEGORI")
     * })
     */
    private $idKategori;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_USER", referencedColumnName="ID_USER")
     * })
     */
    private $idUser;



    /**
     * Set namaAlat
     *
     * @param string $namaAlat
     *
     * @return Alat
     */
    public function setNamaAlat($namaAlat)
    {
        $this->namaAlat = $namaAlat;

        return $this;
    }

    /**
     * Get namaAlat
     *
     * @return string
     */
    public function getNamaAlat()
    {
        return $this->namaAlat;
    }

    /**
     * Set spesifikasi
     *
     * @param string $spesifikasi
     *
     * @return Alat
     */
    public function setSpesifikasi($spesifikasi)
    {
        $this->spesifikasi = $spesifikasi;

        return $this;
    }

    /**
     * Get spesifikasi
     *
     * @return string
     */
    public function getSpesifikasi()
    {
        return $this->spesifikasi;
    }

    /**
     * Set setara
     *
     * @param string $setara
     *
     * @return Alat
     */
    public function setSetara($setara)
    {
        $this->setara = $setara;

        return $this;
    }

    /**
     * Get setara
     *
     * @return string
     */
    public function getSetara()
    {
        return $this->setara;
    }

    /**
     * Set satuan
     *
     * @param string $satuan
     *
     * @return Alat
     */
    public function setSatuan($satuan)
    {
        $this->satuan = $satuan;

        return $this;
    }

    /**
     * Get satuan
     *
     * @return string
     */
    public function getSatuan()
    {
        return $this->satuan;
    }

    /**
     * Set jumlahAlat
     *
     * @param string $jumlahAlat
     *
     * @return Alat
     */
    public function setJumlahAlat($jumlahAlat)
    {
        $this->jumlahAlat = $jumlahAlat;

        return $this;
    }

    /**
     * Get jumlahAlat
     *
     * @return string
     */
    public function getJumlahAlat()
    {
        return $this->jumlahAlat;
    }

    /**
     * Set hargaSatuan
     *
     * @param string $hargaSatuan
     *
     * @return Alat
     */
    public function setHargaSatuan($hargaSatuan)
    {
        $this->hargaSatuan = $hargaSatuan;

        return $this;
    }

    /**
     * Get hargaSatuan
     *
     * @return string
     */
    public function getHargaSatuan()
    {
        return $this->hargaSatuan;
    }

    /**
     * Set jumlahDistribusi
     *
     * @param string $jumlahDistribusi
     *
     * @return Alat
     */
    public function setJumlahDistribusi($jumlahDistribusi)
    {
        $this->jumlahDistribusi = $jumlahDistribusi;

        return $this;
    }

    /**
     * Get jumlahDistribusi
     *
     * @return string
     */
    public function getJumlahDistribusi()
    {
        return $this->jumlahDistribusi;
    }

    /**
     * Set konfirmasi
     *
     * @param string $konfirmasi
     *
     * @return Alat
     */
    public function setKonfirmasi($konfirmasi)
    {
        $this->konfirmasi = $konfirmasi;

        return $this;
    }

    /**
     * Get konfirmasi
     *
     * @return string
     */
    public function getKonfirmasi()
    {
        return $this->konfirmasi;
    }

    /**
     * Set referensiTerkait
     *
     * @param string $referensiTerkait
     *
     * @return Alat
     */
    public function setReferensiTerkait($referensiTerkait)
    {
        $this->referensiTerkait = $referensiTerkait;

        return $this;
    }

    /**
     * Get referensiTerkait
     *
     * @return string
     */
    public function getReferensiTerkait()
    {
        return $this->referensiTerkait;
    }

    /**
     * Set dataAhli
     *
     * @param boolean $dataAhli
     *
     * @return Alat
     */
    public function setDataAhli($dataAhli)
    {
        $this->dataAhli = $dataAhli;

        return $this;
    }

    /**
     * Get dataAhli
     *
     * @return boolean
     */
    public function getDataAhli()
    {
        return $this->dataAhli;
    }

    /**
     * Set revisi
     *
     * @param string $revisi
     *
     * @return Alat
     */
    public function setRevisi($revisi)
    {
        $this->revisi = $revisi;

        return $this;
    }

    /**
     * Get revisi
     *
     * @return string
     */
    public function getRevisi()
    {
        return $this->revisi;
    }

    /**
     * Set tanggalUpdate
     *
     * @param \DateTime $tanggalUpdate
     *
     * @return Alat
     */
    public function setTanggalUpdate($tanggalUpdate)
    {
        $this->tanggalUpdate = $tanggalUpdate;

        return $this;
    }

    /**
     * Get tanggalUpdate
     *
     * @return \DateTime
     */
    public function getTanggalUpdate()
    {
        return $this->tanggalUpdate;
    }

    /**
     * Set noInventaris
     *
     * @param string $noInventaris
     *
     * @return Alat
     */
    public function setNoInventaris($noInventaris)
    {
        $this->noInventaris = $noInventaris;

        return $this;
    }

    /**
     * Get noInventaris
     *
     * @return string
     */
    public function getNoInventaris()
    {
        return $this->noInventaris;
    }

    /**
     * Set isFinal
     *
     * @param boolean $isFinal
     *
     * @return Alat
     */
    public function setIsFinal($isFinal)
    {
        $this->isFinal = $isFinal;

        return $this;
    }

    /**
     * Get isFinal
     *
     * @return boolean
     */
    public function getIsFinal()
    {
        return $this->isFinal;
    }

    /**
     * Set revisiPaket
     *
     * @param string $revisiPaket
     *
     * @return Alat
     */
    public function setRevisiPaket($revisiPaket)
    {
        $this->revisiPaket = $revisiPaket;

        return $this;
    }

    /**
     * Get revisiPaket
     *
     * @return string
     */
    public function getRevisiPaket()
    {
        return $this->revisiPaket;
    }

    /**
     * Get idAlat
     *
     * @return integer
     */
    public function getIdAlat()
    {
        return $this->idAlat;
    }

    /**
     * Set idLokasi
     *
     * @param \AppBundle\Entity\Lokasi $idLokasi
     *
     * @return Alat
     */
    public function setIdLokasi(\AppBundle\Entity\Lokasi $idLokasi = null)
    {
        $this->idLokasi = $idLokasi;

        return $this;
    }

    /**
     * Get idLokasi
     *
     * @return \AppBundle\Entity\Lokasi
     */
    public function getIdLokasi()
    {
        return $this->idLokasi;
    }

    /**
     * Set idFase
     *
     * @param \AppBundle\Entity\Fase $idFase
     *
     * @return Alat
     */
    public function setIdFase(\AppBundle\Entity\Fase $idFase = null)
    {
        $this->idFase = $idFase;

        return $this;
    }

    /**
     * Get idFase
     *
     * @return \AppBundle\Entity\Fase
     */
    public function getIdFase()
    {
        return $this->idFase;
    }

    /**
     * Set idJurusan
     *
     * @param \AppBundle\Entity\Jurusan $idJurusan
     *
     * @return Alat
     */
    public function setIdJurusan(\AppBundle\Entity\Jurusan $idJurusan = null)
    {
        $this->idJurusan = $idJurusan;

        return $this;
    }

    /**
     * Get idJurusan
     *
     * @return \AppBundle\Entity\Jurusan
     */
    public function getIdJurusan()
    {
        return $this->idJurusan;
    }

    /**
     * Set idPaket
     *
     * @param \AppBundle\Entity\Paket $idPaket
     *
     * @return Alat
     */
    public function setIdPaket(\AppBundle\Entity\Paket $idPaket = null)
    {
        $this->idPaket = $idPaket;

        return $this;
    }

    /**
     * Get idPaket
     *
     * @return \AppBundle\Entity\Paket
     */
    public function getIdPaket()
    {
        return $this->idPaket;
    }

    /**
     * Set idUsulan
     *
     * @param \AppBundle\Entity\Usulan $idUsulan
     *
     * @return Alat
     */
    public function setIdUsulan(\AppBundle\Entity\Usulan $idUsulan = null)
    {
        $this->idUsulan = $idUsulan;

        return $this;
    }

    /**
     * Get idUsulan
     *
     * @return \AppBundle\Entity\Usulan
     */
    public function getIdUsulan()
    {
        return $this->idUsulan;
    }

    /**
     * Set idKategori
     *
     * @param \AppBundle\Entity\Kategori $idKategori
     *
     * @return Alat
     */
    public function setIdKategori(\AppBundle\Entity\Kategori $idKategori = null)
    {
        $this->idKategori = $idKategori;

        return $this;
    }

    /**
     * Get idKategori
     *
     * @return \AppBundle\Entity\Kategori
     */
    public function getIdKategori()
    {
        return $this->idKategori;
    }

    /**
     * Set idUser
     *
     * @param \AppBundle\Entity\User $idUser
     *
     * @return Alat
     */
    public function setIdUser(\AppBundle\Entity\User $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdUser()
    {
        return $this->idUser;
    }
}
