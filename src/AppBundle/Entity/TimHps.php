<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TimHps
 *
 * @ORM\Table(name="tim_hps", indexes={@ORM\Index(name="FK_RELATIONSHIP_32", columns={"ID_USER"})})
 * @ORM\Entity
 */
class TimHps
{
    /**
     * @var string
     *
     * @ORM\Column(name="NAMA_TIM", type="string", length=50, nullable=true)
     */
    private $namaTim;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_TIM_HPS", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idTimHps;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_USER", referencedColumnName="ID_USER")
     * })
     */
    private $idUser;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Pegawai", inversedBy="idTimHps")
     * @ORM\JoinTable(name="anggota_tim_hps",
     *   joinColumns={
     *     @ORM\JoinColumn(name="ID_TIM_HPS", referencedColumnName="ID_TIM_HPS")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="NIP", referencedColumnName="NIP")
     *   }
     * )
     */
    private $nip;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->nip = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set namaTim
     *
     * @param string $namaTim
     *
     * @return TimHps
     */
    public function setNamaTim($namaTim)
    {
        $this->namaTim = $namaTim;

        return $this;
    }

    /**
     * Get namaTim
     *
     * @return string
     */
    public function getNamaTim()
    {
        return $this->namaTim;
    }

    /**
     * Get idTimHps
     *
     * @return integer
     */
    public function getIdTimHps()
    {
        return $this->idTimHps;
    }

    /**
     * Set idUser
     *
     * @param \AppBundle\Entity\User $idUser
     *
     * @return TimHps
     */
    public function setIdUser(\AppBundle\Entity\User $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Add nip
     *
     * @param \AppBundle\Entity\Pegawai $nip
     *
     * @return TimHps
     */
    public function addNip(\AppBundle\Entity\Pegawai $nip)
    {
        $this->nip[] = $nip;

        return $this;
    }

    /**
     * Remove nip
     *
     * @param \AppBundle\Entity\Pegawai $nip
     */
    public function removeNip(\AppBundle\Entity\Pegawai $nip)
    {
        $this->nip->removeElement($nip);
    }

    /**
     * Get nip
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNip()
    {
        return $this->nip;
    }
}
