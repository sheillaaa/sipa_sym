<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pagu
 *
 * @ORM\Table(name="pagu", indexes={@ORM\Index(name="FK_RELATIONSHIP_17", columns={"ID_JURUSAN"}), @ORM\Index(name="FK_RELATIONSHIP_46", columns={"TAHUN_ANGGARAN"})})
 * @ORM\Entity
 */
class Pagu
{
    /**
     * @var integer
     *
     * @ORM\Column(name="PAGU_ALAT", type="bigint", nullable=true)
     */
    private $paguAlat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="TANGGAL", type="datetime", nullable=true)
     */
    private $tanggal;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_PAGU", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPagu;

    /**
     * @var \AppBundle\Entity\Jurusan
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Jurusan")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_JURUSAN", referencedColumnName="ID_JURUSAN")
     * })
     */
    private $idJurusan;

    /**
     * @var \AppBundle\Entity\TahunAnggaran
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TahunAnggaran")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="TAHUN_ANGGARAN", referencedColumnName="TAHUN_ANGGARAN")
     * })
     */
    private $tahunAnggaran;



    /**
     * Set paguAlat
     *
     * @param integer $paguAlat
     *
     * @return Pagu
     */
    public function setPaguAlat($paguAlat)
    {
        $this->paguAlat = $paguAlat;

        return $this;
    }

    /**
     * Get paguAlat
     *
     * @return integer
     */
    public function getPaguAlat()
    {
        return $this->paguAlat;
    }

    /**
     * Set tanggal
     *
     * @param \DateTime $tanggal
     *
     * @return Pagu
     */
    public function setTanggal($tanggal)
    {
        $this->tanggal = $tanggal;

        return $this;
    }

    /**
     * Get tanggal
     *
     * @return \DateTime
     */
    public function getTanggal()
    {
        return $this->tanggal;
    }

    /**
     * Get idPagu
     *
     * @return integer
     */
    public function getIdPagu()
    {
        return $this->idPagu;
    }

    /**
     * Set idJurusan
     *
     * @param \AppBundle\Entity\Jurusan $idJurusan
     *
     * @return Pagu
     */
    public function setIdJurusan(\AppBundle\Entity\Jurusan $idJurusan = null)
    {
        $this->idJurusan = $idJurusan;

        return $this;
    }

    /**
     * Get idJurusan
     *
     * @return \AppBundle\Entity\Jurusan
     */
    public function getIdJurusan()
    {
        return $this->idJurusan;
    }

    /**
     * Set tahunAnggaran
     *
     * @param \AppBundle\Entity\TahunAnggaran $tahunAnggaran
     *
     * @return Pagu
     */
    public function setTahunAnggaran(\AppBundle\Entity\TahunAnggaran $tahunAnggaran = null)
    {
        $this->tahunAnggaran = $tahunAnggaran;

        return $this;
    }

    /**
     * Get tahunAnggaran
     *
     * @return \AppBundle\Entity\TahunAnggaran
     */
    public function getTahunAnggaran()
    {
        return $this->tahunAnggaran;
    }
}
