<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TimPenerima
 *
 * @ORM\Table(name="tim_penerima", indexes={@ORM\Index(name="FK_RELATIONSHIP_33", columns={"ID_USER"})})
 * @ORM\Entity
 */
class TimPenerima
{
    /**
     * @var string
     *
     * @ORM\Column(name="NAMA_TIM", type="string", length=50, nullable=true)
     */
    private $namaTim;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_TIM_PENERIMA", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idTimPenerima;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_USER", referencedColumnName="ID_USER")
     * })
     */
    private $idUser;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Pegawai", inversedBy="idTimPenerima")
     * @ORM\JoinTable(name="anggota_tim_penerima",
     *   joinColumns={
     *     @ORM\JoinColumn(name="ID_TIM_PENERIMA", referencedColumnName="ID_TIM_PENERIMA")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="NIP", referencedColumnName="NIP")
     *   }
     * )
     */
    private $nip;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->nip = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set namaTim
     *
     * @param string $namaTim
     *
     * @return TimPenerima
     */
    public function setNamaTim($namaTim)
    {
        $this->namaTim = $namaTim;

        return $this;
    }

    /**
     * Get namaTim
     *
     * @return string
     */
    public function getNamaTim()
    {
        return $this->namaTim;
    }

    /**
     * Get idTimPenerima
     *
     * @return integer
     */
    public function getIdTimPenerima()
    {
        return $this->idTimPenerima;
    }

    /**
     * Set idUser
     *
     * @param \AppBundle\Entity\User $idUser
     *
     * @return TimPenerima
     */
    public function setIdUser(\AppBundle\Entity\User $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Add nip
     *
     * @param \AppBundle\Entity\Pegawai $nip
     *
     * @return TimPenerima
     */
    public function addNip(\AppBundle\Entity\Pegawai $nip)
    {
        $this->nip[] = $nip;

        return $this;
    }

    /**
     * Remove nip
     *
     * @param \AppBundle\Entity\Pegawai $nip
     */
    public function removeNip(\AppBundle\Entity\Pegawai $nip)
    {
        $this->nip->removeElement($nip);
    }

    /**
     * Get nip
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNip()
    {
        return $this->nip;
    }
}
