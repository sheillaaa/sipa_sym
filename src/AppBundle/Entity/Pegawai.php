<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pegawai
 *
 * @ORM\Table(name="pegawai")
 * @ORM\Entity
 */
class Pegawai
{
    /**
     * @var string
     *
     * @ORM\Column(name="NAMA_PEGAWAI", type="string", length=50, nullable=true)
     */
    private $namaPegawai;

    /**
     * @var string
     *
     * @ORM\Column(name="NO_HP", type="decimal", precision=13, scale=0, nullable=true)
     */
    private $noHp;

    /**
     * @var string
     *
     * @ORM\Column(name="EMAIL", type="string", length=50, nullable=true)
     */
    private $email;

    /**
     * @var boolean
     *
     * @ORM\Column(name="IS_TEKNISI", type="boolean", nullable=true)
     */
    private $isTeknisi;

    /**
     * @var boolean
     *
     * @ORM\Column(name="HPS_CERTIFIED", type="boolean", nullable=true)
     */
    private $hpsCertified;

    /**
     * @var string
     *
     * @ORM\Column(name="NIP", type="string", length=20)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $nip;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\TimHps", mappedBy="nip")
     */
    private $idTimHps;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\TimPenerima", mappedBy="nip")
     */
    private $idTimPenerima;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idTimHps = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idTimPenerima = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set namaPegawai
     *
     * @param string $namaPegawai
     *
     * @return Pegawai
     */
    public function setNamaPegawai($namaPegawai)
    {
        $this->namaPegawai = $namaPegawai;

        return $this;
    }

    /**
     * Get namaPegawai
     *
     * @return string
     */
    public function getNamaPegawai()
    {
        return $this->namaPegawai;
    }

    /**
     * Set noHp
     *
     * @param string $noHp
     *
     * @return Pegawai
     */
    public function setNoHp($noHp)
    {
        $this->noHp = $noHp;

        return $this;
    }

    /**
     * Get noHp
     *
     * @return string
     */
    public function getNoHp()
    {
        return $this->noHp;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Pegawai
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set isTeknisi
     *
     * @param boolean $isTeknisi
     *
     * @return Pegawai
     */
    public function setIsTeknisi($isTeknisi)
    {
        $this->isTeknisi = $isTeknisi;

        return $this;
    }

    /**
     * Get isTeknisi
     *
     * @return boolean
     */
    public function getIsTeknisi()
    {
        return $this->isTeknisi;
    }

    /**
     * Set hpsCertified
     *
     * @param boolean $hpsCertified
     *
     * @return Pegawai
     */
    public function setHpsCertified($hpsCertified)
    {
        $this->hpsCertified = $hpsCertified;

        return $this;
    }

    /**
     * Get hpsCertified
     *
     * @return boolean
     */
    public function getHpsCertified()
    {
        return $this->hpsCertified;
    }

    /**
     * Get nip
     *
     * @return string
     */
    public function getNip()
    {
        return $this->nip;
    }

    /**
     * Add idTimHp
     *
     * @param \AppBundle\Entity\TimHps $idTimHp
     *
     * @return Pegawai
     */
    public function addIdTimHp(\AppBundle\Entity\TimHps $idTimHp)
    {
        $this->idTimHps[] = $idTimHp;

        return $this;
    }

    /**
     * Remove idTimHp
     *
     * @param \AppBundle\Entity\TimHps $idTimHp
     */
    public function removeIdTimHp(\AppBundle\Entity\TimHps $idTimHp)
    {
        $this->idTimHps->removeElement($idTimHp);
    }

    /**
     * Get idTimHps
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdTimHps()
    {
        return $this->idTimHps;
    }

    /**
     * Add idTimPenerima
     *
     * @param \AppBundle\Entity\TimPenerima $idTimPenerima
     *
     * @return Pegawai
     */
    public function addIdTimPenerima(\AppBundle\Entity\TimPenerima $idTimPenerima)
    {
        $this->idTimPenerima[] = $idTimPenerima;

        return $this;
    }

    /**
     * Remove idTimPenerima
     *
     * @param \AppBundle\Entity\TimPenerima $idTimPenerima
     */
    public function removeIdTimPenerima(\AppBundle\Entity\TimPenerima $idTimPenerima)
    {
        $this->idTimPenerima->removeElement($idTimPenerima);
    }

    /**
     * Get idTimPenerima
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdTimPenerima()
    {
        return $this->idTimPenerima;
    }
}
