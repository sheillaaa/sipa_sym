<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TahunAnggaran
 *
 * @ORM\Table(name="tahun_anggaran")
 * @ORM\Entity
 */
class TahunAnggaran
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="TANGGAL_MULAI", type="date", nullable=true)
     */
    private $tanggalMulai;

    /**
     * @var integer
     *
     * @ORM\Column(name="TAHUN_ANGGARAN", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tahunAnggaran;



    /**
     * Set tanggalMulai
     *
     * @param \DateTime $tanggalMulai
     *
     * @return TahunAnggaran
     */
    public function setTanggalMulai($tanggalMulai)
    {
        $this->tanggalMulai = $tanggalMulai;

        return $this;
    }

    /**
     * Get tanggalMulai
     *
     * @return \DateTime
     */
    public function getTanggalMulai()
    {
        return $this->tanggalMulai;
    }

    /**
     * Get tahunAnggaran
     *
     * @return integer
     */
    public function getTahunAnggaran()
    {
        return $this->tahunAnggaran;
    }
}
