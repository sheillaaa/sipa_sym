<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="user", indexes={@ORM\Index(name="FK_RELATIONSHIP_1", columns={"ID_JENIS_USER"}), @ORM\Index(name="FK_RELATIONSHIP_23", columns={"ID_JURUSAN"}), @ORM\Index(name="FK_RELATIONSHIP_31", columns={"NIP"})})
 * @ORM\Entity
 */
class User
{
    /**
     * @var string
     *
     * @ORM\Column(name="USERNAME", type="string", length=50, nullable=true)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="PASSWORD", type="string", length=50, nullable=true)
     */
    private $password;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_USER", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idUser;

    /**
     * @var \AppBundle\Entity\JenisUser
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\JenisUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_JENIS_USER", referencedColumnName="ID_JENIS_USER")
     * })
     */
    private $idJenisUser;

    /**
     * @var \AppBundle\Entity\Jurusan
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Jurusan")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_JURUSAN", referencedColumnName="ID_JURUSAN")
     * })
     */
    private $idJurusan;

    /**
     * @var \AppBundle\Entity\Pegawai
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Pegawai")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="NIP", referencedColumnName="NIP")
     * })
     */
    private $nip;



    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Get idUser
     *
     * @return integer
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set idJenisUser
     *
     * @param \AppBundle\Entity\JenisUser $idJenisUser
     *
     * @return User
     */
    public function setIdJenisUser(\AppBundle\Entity\JenisUser $idJenisUser = null)
    {
        $this->idJenisUser = $idJenisUser;

        return $this;
    }

    /**
     * Get idJenisUser
     *
     * @return \AppBundle\Entity\JenisUser
     */
    public function getIdJenisUser()
    {
        return $this->idJenisUser;
    }

    /**
     * Set idJurusan
     *
     * @param \AppBundle\Entity\Jurusan $idJurusan
     *
     * @return User
     */
    public function setIdJurusan(\AppBundle\Entity\Jurusan $idJurusan = null)
    {
        $this->idJurusan = $idJurusan;

        return $this;
    }

    /**
     * Get idJurusan
     *
     * @return \AppBundle\Entity\Jurusan
     */
    public function getIdJurusan()
    {
        return $this->idJurusan;
    }

    /**
     * Set nip
     *
     * @param \AppBundle\Entity\Pegawai $nip
     *
     * @return User
     */
    public function setNip(\AppBundle\Entity\Pegawai $nip = null)
    {
        $this->nip = $nip;

        return $this;
    }

    /**
     * Get nip
     *
     * @return \AppBundle\Entity\Pegawai
     */
    public function getNip()
    {
        return $this->nip;
    }
}
