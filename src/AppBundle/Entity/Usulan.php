<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Usulan
 *
 * @ORM\Table(name="usulan", indexes={@ORM\Index(name="FK_RELATIONSHIP_27", columns={"ID_USER"}), @ORM\Index(name="FK_RELATIONSHIP_30", columns={"ID_JURUSAN"}), @ORM\Index(name="FK_RELATIONSHIP_45", columns={"TAHUN_ANGGARAN"})})
 * @ORM\Entity
 */
class Usulan
{
    /**
     * @var string
     *
     * @ORM\Column(name="NAMA_PAKET", type="text", length=65535, nullable=true)
     */
    private $namaPaket;

    /**
     * @var string
     *
     * @ORM\Column(name="STATUS", type="decimal", precision=8, scale=0, nullable=true)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="TANGGAL_DIBUAT", type="datetime", nullable=true)
     */
    private $tanggalDibuat;

    /**
     * @var integer
     *
     * @ORM\Column(name="TOTAL_ANGGARAN", type="bigint", nullable=true)
     */
    private $totalAnggaran;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="LAST_UPDATE", type="datetime", nullable=true)
     */
    private $lastUpdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_USULAN", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idUsulan;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_USER", referencedColumnName="ID_USER")
     * })
     */
    private $idUser;

    /**
     * @var \AppBundle\Entity\Jurusan
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Jurusan")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_JURUSAN", referencedColumnName="ID_JURUSAN")
     * })
     */
    private $idJurusan;

    /**
     * @var \AppBundle\Entity\TahunAnggaran
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TahunAnggaran")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="TAHUN_ANGGARAN", referencedColumnName="TAHUN_ANGGARAN")
     * })
     */
    private $tahunAnggaran;



    /**
     * Set namaPaket
     *
     * @param string $namaPaket
     *
     * @return Usulan
     */
    public function setNamaPaket($namaPaket)
    {
        $this->namaPaket = $namaPaket;

        return $this;
    }

    /**
     * Get namaPaket
     *
     * @return string
     */
    public function getNamaPaket()
    {
        return $this->namaPaket;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Usulan
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set tanggalDibuat
     *
     * @param \DateTime $tanggalDibuat
     *
     * @return Usulan
     */
    public function setTanggalDibuat($tanggalDibuat)
    {
        $this->tanggalDibuat = $tanggalDibuat;

        return $this;
    }

    /**
     * Get tanggalDibuat
     *
     * @return \DateTime
     */
    public function getTanggalDibuat()
    {
        return $this->tanggalDibuat;
    }

    /**
     * Set totalAnggaran
     *
     * @param integer $totalAnggaran
     *
     * @return Usulan
     */
    public function setTotalAnggaran($totalAnggaran)
    {
        $this->totalAnggaran = $totalAnggaran;

        return $this;
    }

    /**
     * Get totalAnggaran
     *
     * @return integer
     */
    public function getTotalAnggaran()
    {
        return $this->totalAnggaran;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return Usulan
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Get idUsulan
     *
     * @return integer
     */
    public function getIdUsulan()
    {
        return $this->idUsulan;
    }

    /**
     * Set idUser
     *
     * @param \AppBundle\Entity\User $idUser
     *
     * @return Usulan
     */
    public function setIdUser(\AppBundle\Entity\User $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set idJurusan
     *
     * @param \AppBundle\Entity\Jurusan $idJurusan
     *
     * @return Usulan
     */
    public function setIdJurusan(\AppBundle\Entity\Jurusan $idJurusan = null)
    {
        $this->idJurusan = $idJurusan;

        return $this;
    }

    /**
     * Get idJurusan
     *
     * @return \AppBundle\Entity\Jurusan
     */
    public function getIdJurusan()
    {
        return $this->idJurusan;
    }

    /**
     * Set tahunAnggaran
     *
     * @param \AppBundle\Entity\TahunAnggaran $tahunAnggaran
     *
     * @return Usulan
     */
    public function setTahunAnggaran(\AppBundle\Entity\TahunAnggaran $tahunAnggaran = null)
    {
        $this->tahunAnggaran = $tahunAnggaran;

        return $this;
    }

    /**
     * Get tahunAnggaran
     *
     * @return \AppBundle\Entity\TahunAnggaran
     */
    public function getTahunAnggaran()
    {
        return $this->tahunAnggaran;
    }
}
