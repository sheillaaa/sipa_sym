<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Kontrak
 *
 * @ORM\Table(name="kontrak", indexes={@ORM\Index(name="FK_RELATIONSHIP_24", columns={"ID_USER"}), @ORM\Index(name="FK_RELATIONSHIP_25", columns={"ID_PAKET"})})
 * @ORM\Entity
 */
class Kontrak
{
    /**
     * @var string
     *
     * @ORM\Column(name="KETERANGAN", type="text", length=65535, nullable=true)
     */
    private $keterangan;

    /**
     * @var string
     *
     * @ORM\Column(name="FILE", type="text", length=65535, nullable=true)
     */
    private $file;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_KONTRAK", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idKontrak;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_USER", referencedColumnName="ID_USER")
     * })
     */
    private $idUser;

    /**
     * @var \AppBundle\Entity\Paket
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Paket")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_PAKET", referencedColumnName="ID_PAKET")
     * })
     */
    private $idPaket;



    /**
     * Set keterangan
     *
     * @param string $keterangan
     *
     * @return Kontrak
     */
    public function setKeterangan($keterangan)
    {
        $this->keterangan = $keterangan;

        return $this;
    }

    /**
     * Get keterangan
     *
     * @return string
     */
    public function getKeterangan()
    {
        return $this->keterangan;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     * @return Kontrak
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Get idKontrak
     *
     * @return integer
     */
    public function getIdKontrak()
    {
        return $this->idKontrak;
    }

    /**
     * Set idUser
     *
     * @param \AppBundle\Entity\User $idUser
     *
     * @return Kontrak
     */
    public function setIdUser(\AppBundle\Entity\User $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set idPaket
     *
     * @param \AppBundle\Entity\Paket $idPaket
     *
     * @return Kontrak
     */
    public function setIdPaket(\AppBundle\Entity\Paket $idPaket = null)
    {
        $this->idPaket = $idPaket;

        return $this;
    }

    /**
     * Get idPaket
     *
     * @return \AppBundle\Entity\Paket
     */
    public function getIdPaket()
    {
        return $this->idPaket;
    }
}
