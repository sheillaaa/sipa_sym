<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * JenisUser
 *
 * @ORM\Table(name="jenis_user")
 * @ORM\Entity
 */
class JenisUser
{
    /**
     * @var string
     *
     * @ORM\Column(name="NAMA_JENIS_USER", type="string", length=50, nullable=true)
     */
    private $namaJenisUser;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_JENIS_USER", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idJenisUser;



    /**
     * Set namaJenisUser
     *
     * @param string $namaJenisUser
     *
     * @return JenisUser
     */
    public function setNamaJenisUser($namaJenisUser)
    {
        $this->namaJenisUser = $namaJenisUser;

        return $this;
    }

    /**
     * Get namaJenisUser
     *
     * @return string
     */
    public function getNamaJenisUser()
    {
        return $this->namaJenisUser;
    }

    /**
     * Get idJenisUser
     *
     * @return integer
     */
    public function getIdJenisUser()
    {
        return $this->idJenisUser;
    }
}
