<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Penerimaan
 *
 * @ORM\Table(name="penerimaan", indexes={@ORM\Index(name="FK_RELATIONSHIP_37", columns={"ID_PAKET"}), @ORM\Index(name="FK_RELATIONSHIP_8", columns={"ID_TIM_PENERIMA"}), @ORM\Index(name="FK_RELATIONSHIP_9", columns={"ID_ALAT"})})
 * @ORM\Entity
 */
class Penerimaan
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="TANGGAL_PENERIMAAN", type="datetime", nullable=true)
     */
    private $tanggalPenerimaan;

    /**
     * @var string
     *
     * @ORM\Column(name="JUMLAH", type="decimal", precision=8, scale=0, nullable=true)
     */
    private $jumlah;

    /**
     * @var string
     *
     * @ORM\Column(name="KETERANGAN", type="text", length=65535, nullable=true)
     */
    private $keterangan;

    /**
     * @var boolean
     *
     * @ORM\Column(name="STATUS_KONFIRMASI", type="boolean", nullable=true)
     */
    private $statusKonfirmasi;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_PENERIMAAN", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPenerimaan;

    /**
     * @var \AppBundle\Entity\Paket
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Paket")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_PAKET", referencedColumnName="ID_PAKET")
     * })
     */
    private $idPaket;

    /**
     * @var \AppBundle\Entity\TimPenerima
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TimPenerima")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_TIM_PENERIMA", referencedColumnName="ID_TIM_PENERIMA")
     * })
     */
    private $idTimPenerima;

    /**
     * @var \AppBundle\Entity\Alat
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Alat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_ALAT", referencedColumnName="ID_ALAT")
     * })
     */
    private $idAlat;



    /**
     * Set tanggalPenerimaan
     *
     * @param \DateTime $tanggalPenerimaan
     *
     * @return Penerimaan
     */
    public function setTanggalPenerimaan($tanggalPenerimaan)
    {
        $this->tanggalPenerimaan = $tanggalPenerimaan;

        return $this;
    }

    /**
     * Get tanggalPenerimaan
     *
     * @return \DateTime
     */
    public function getTanggalPenerimaan()
    {
        return $this->tanggalPenerimaan;
    }

    /**
     * Set jumlah
     *
     * @param string $jumlah
     *
     * @return Penerimaan
     */
    public function setJumlah($jumlah)
    {
        $this->jumlah = $jumlah;

        return $this;
    }

    /**
     * Get jumlah
     *
     * @return string
     */
    public function getJumlah()
    {
        return $this->jumlah;
    }

    /**
     * Set keterangan
     *
     * @param string $keterangan
     *
     * @return Penerimaan
     */
    public function setKeterangan($keterangan)
    {
        $this->keterangan = $keterangan;

        return $this;
    }

    /**
     * Get keterangan
     *
     * @return string
     */
    public function getKeterangan()
    {
        return $this->keterangan;
    }

    /**
     * Set statusKonfirmasi
     *
     * @param boolean $statusKonfirmasi
     *
     * @return Penerimaan
     */
    public function setStatusKonfirmasi($statusKonfirmasi)
    {
        $this->statusKonfirmasi = $statusKonfirmasi;

        return $this;
    }

    /**
     * Get statusKonfirmasi
     *
     * @return boolean
     */
    public function getStatusKonfirmasi()
    {
        return $this->statusKonfirmasi;
    }

    /**
     * Get idPenerimaan
     *
     * @return integer
     */
    public function getIdPenerimaan()
    {
        return $this->idPenerimaan;
    }

    /**
     * Set idPaket
     *
     * @param \AppBundle\Entity\Paket $idPaket
     *
     * @return Penerimaan
     */
    public function setIdPaket(\AppBundle\Entity\Paket $idPaket = null)
    {
        $this->idPaket = $idPaket;

        return $this;
    }

    /**
     * Get idPaket
     *
     * @return \AppBundle\Entity\Paket
     */
    public function getIdPaket()
    {
        return $this->idPaket;
    }

    /**
     * Set idTimPenerima
     *
     * @param \AppBundle\Entity\TimPenerima $idTimPenerima
     *
     * @return Penerimaan
     */
    public function setIdTimPenerima(\AppBundle\Entity\TimPenerima $idTimPenerima = null)
    {
        $this->idTimPenerima = $idTimPenerima;

        return $this;
    }

    /**
     * Get idTimPenerima
     *
     * @return \AppBundle\Entity\TimPenerima
     */
    public function getIdTimPenerima()
    {
        return $this->idTimPenerima;
    }

    /**
     * Set idAlat
     *
     * @param \AppBundle\Entity\Alat $idAlat
     *
     * @return Penerimaan
     */
    public function setIdAlat(\AppBundle\Entity\Alat $idAlat = null)
    {
        $this->idAlat = $idAlat;

        return $this;
    }

    /**
     * Get idAlat
     *
     * @return \AppBundle\Entity\Alat
     */
    public function getIdAlat()
    {
        return $this->idAlat;
    }
}
