<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pemenang
 *
 * @ORM\Table(name="pemenang", indexes={@ORM\Index(name="FK_RELATIONSHIP_47", columns={"ID_PAKET"})})
 * @ORM\Entity
 */
class Pemenang
{
    /**
     * @var string
     *
     * @ORM\Column(name="NAMA_PERUSAHAAN", type="string", length=100, nullable=true)
     */
    private $namaPerusahaan;

    /**
     * @var string
     *
     * @ORM\Column(name="NPWP", type="string", length=25, nullable=true)
     */
    private $npwp;

    /**
     * @var string
     *
     * @ORM\Column(name="ALAMAT", type="text", length=65535, nullable=true)
     */
    private $alamat;

    /**
     * @var string
     *
     * @ORM\Column(name="PIC_PERUSAHAAN", type="string", length=50, nullable=true)
     */
    private $picPerusahaan;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_PEMENANG", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPemenang;

    /**
     * @var \AppBundle\Entity\Paket
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Paket")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_PAKET", referencedColumnName="ID_PAKET")
     * })
     */
    private $idPaket;



    /**
     * Set namaPerusahaan
     *
     * @param string $namaPerusahaan
     *
     * @return Pemenang
     */
    public function setNamaPerusahaan($namaPerusahaan)
    {
        $this->namaPerusahaan = $namaPerusahaan;

        return $this;
    }

    /**
     * Get namaPerusahaan
     *
     * @return string
     */
    public function getNamaPerusahaan()
    {
        return $this->namaPerusahaan;
    }

    /**
     * Set npwp
     *
     * @param string $npwp
     *
     * @return Pemenang
     */
    public function setNpwp($npwp)
    {
        $this->npwp = $npwp;

        return $this;
    }

    /**
     * Get npwp
     *
     * @return string
     */
    public function getNpwp()
    {
        return $this->npwp;
    }

    /**
     * Set alamat
     *
     * @param string $alamat
     *
     * @return Pemenang
     */
    public function setAlamat($alamat)
    {
        $this->alamat = $alamat;

        return $this;
    }

    /**
     * Get alamat
     *
     * @return string
     */
    public function getAlamat()
    {
        return $this->alamat;
    }

    /**
     * Set picPerusahaan
     *
     * @param string $picPerusahaan
     *
     * @return Pemenang
     */
    public function setPicPerusahaan($picPerusahaan)
    {
        $this->picPerusahaan = $picPerusahaan;

        return $this;
    }

    /**
     * Get picPerusahaan
     *
     * @return string
     */
    public function getPicPerusahaan()
    {
        return $this->picPerusahaan;
    }

    /**
     * Get idPemenang
     *
     * @return integer
     */
    public function getIdPemenang()
    {
        return $this->idPemenang;
    }

    /**
     * Set idPaket
     *
     * @param \AppBundle\Entity\Paket $idPaket
     *
     * @return Pemenang
     */
    public function setIdPaket(\AppBundle\Entity\Paket $idPaket = null)
    {
        $this->idPaket = $idPaket;

        return $this;
    }

    /**
     * Get idPaket
     *
     * @return \AppBundle\Entity\Paket
     */
    public function getIdPaket()
    {
        return $this->idPaket;
    }
}
