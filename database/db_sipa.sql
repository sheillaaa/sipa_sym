-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 27, 2016 at 11:46 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sipa`
--

-- --------------------------------------------------------

--
-- Table structure for table `alat`
--

CREATE TABLE `alat` (
  `ID_ALAT` int(11) NOT NULL,
  `ID_JURUSAN` int(11) DEFAULT NULL,
  `ID_USER` int(11) DEFAULT NULL,
  `ID_LOKASI` int(11) DEFAULT NULL,
  `ID_KATEGORI` int(11) DEFAULT NULL,
  `ID_PAKET` int(11) DEFAULT NULL,
  `ID_FASE` int(11) DEFAULT NULL,
  `ID_USULAN` int(11) DEFAULT NULL,
  `NAMA_ALAT` varchar(50) DEFAULT NULL,
  `SPESIFIKASI` text,
  `SETARA` text,
  `SATUAN` varchar(20) DEFAULT NULL,
  `JUMLAH_ALAT` decimal(8,0) DEFAULT NULL,
  `HARGA_SATUAN` decimal(8,0) DEFAULT NULL,
  `JUMLAH_DISTRIBUSI` decimal(8,0) DEFAULT NULL,
  `KONFIRMASI` text,
  `REFERENSI_TERKAIT` text,
  `DATA_AHLI` tinyint(1) DEFAULT NULL,
  `REVISI` decimal(8,0) DEFAULT NULL,
  `TANGGAL_UPDATE` datetime DEFAULT NULL,
  `NO_INVENTARIS` decimal(8,0) DEFAULT NULL,
  `IS_FINAL` tinyint(1) DEFAULT NULL,
  `REVISI_PAKET` decimal(8,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `anggota_tim_hps`
--

CREATE TABLE `anggota_tim_hps` (
  `ID_TIM_HPS` int(11) NOT NULL,
  `NIP` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `anggota_tim_penerima`
--

CREATE TABLE `anggota_tim_penerima` (
  `ID_TIM_PENERIMA` int(11) NOT NULL,
  `NIP` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bukti_pengadaan`
--

CREATE TABLE `bukti_pengadaan` (
  `ID_BUKTI` int(11) NOT NULL,
  `ID_PAKET` int(11) DEFAULT NULL,
  `TANGGAL` datetime DEFAULT NULL,
  `FILE` text,
  `KETERANGAN` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fase`
--

CREATE TABLE `fase` (
  `ID_FASE` int(11) NOT NULL,
  `NAMA_FASE` varchar(50) DEFAULT NULL,
  `WAKTU_PELAKSANAAN` int(3) NOT NULL,
  `WAKTU_TAMBAHAN` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fase`
--

INSERT INTO `fase` (`ID_FASE`, `NAMA_FASE`, `WAKTU_PELAKSANAAN`, `WAKTU_TAMBAHAN`) VALUES
(1, 'Pengajuan', 30, 16),
(2, 'VerifikasiHPS', 10, 4),
(3, 'Pengadaan', 30, 15),
(4, 'PenetapanKontrak', 7, 3),
(5, 'Penerimaan', 9, 9),
(6, 'Pencatatan', 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_user`
--

CREATE TABLE `jenis_user` (
  `ID_JENIS_USER` int(11) NOT NULL,
  `NAMA_JENIS_USER` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_user`
--

INSERT INTO `jenis_user` (`ID_JENIS_USER`, `NAMA_JENIS_USER`) VALUES
(1, 'Teknisi'),
(2, 'Kepala Lab'),
(3, 'Manajemen'),
(4, 'Pembantu Direktur 2'),
(5, 'PPK'),
(6, 'Tim HPS'),
(7, 'ULP'),
(8, 'Tim Penerima'),
(9, 'PPSPM'),
(10, 'Direktur'),
(11, 'Administrasi Umum'),
(99, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `jurusan`
--

CREATE TABLE `jurusan` (
  `ID_JURUSAN` int(11) NOT NULL,
  `NAMA_JURUSAN` varchar(50) DEFAULT NULL,
  `INISIAL` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurusan`
--

INSERT INTO `jurusan` (`ID_JURUSAN`, `NAMA_JURUSAN`, `INISIAL`) VALUES
(0, NULL, NULL),
(1, 'Teknik Komputer dan Informatika', 'JTK'),
(2, 'Administrasi Niaga', 'AN'),
(3, 'Teknik Sipil', 'Sipil'),
(4, 'Teknik Mesin', 'Mesin'),
(5, 'Teknik Refrigerasi dan Tata Udara', 'Refri'),
(6, 'Teknik Konversi Energi', 'Energi'),
(7, 'Teknik Elektro', 'JTE'),
(8, 'Teknik Kimia', 'KI'),
(9, 'Akuntansi', 'Akun'),
(10, 'Bahasa Inggris', 'BI');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `ID_KATEGORI` int(11) NOT NULL,
  `KATEGORI` varchar(50) DEFAULT NULL,
  `KETERANGAN` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`ID_KATEGORI`, `KATEGORI`, `KETERANGAN`) VALUES
(1, 'Bahan Kimia', ''),
(2, 'Alat Berat', ''),
(3, 'Kendaraan Bermotor', ''),
(4, 'Alat Kebersihan', ''),
(5, 'Material Konstruksi', ''),
(6, 'Tata Lingkungan', ''),
(7, 'Internet Service Provider', ''),
(8, 'Komunikasi & Informatika', ''),
(9, 'Peralatan Kantor', '');

-- --------------------------------------------------------

--
-- Table structure for table `kontrak`
--

CREATE TABLE `kontrak` (
  `ID_KONTRAK` int(11) NOT NULL,
  `ID_PAKET` int(11) DEFAULT NULL,
  `ID_USER` int(11) DEFAULT NULL,
  `KETERANGAN` text,
  `FILE` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lokasi`
--

CREATE TABLE `lokasi` (
  `ID_LOKASI` int(11) NOT NULL,
  `ID_JURUSAN` int(11) DEFAULT NULL,
  `NAMA_LOKASI` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lokasi`
--

INSERT INTO `lokasi` (`ID_LOKASI`, `ID_JURUSAN`, `NAMA_LOKASI`) VALUES
(1, 1, 'RSG'),
(2, 1, 'Lab. RPL'),
(3, 2, 'Lab Bisnis'),
(4, 2, 'Ruang Rapat');

-- --------------------------------------------------------

--
-- Table structure for table `pagu`
--

CREATE TABLE `pagu` (
  `ID_PAGU` int(11) NOT NULL,
  `ID_JURUSAN` int(11) DEFAULT NULL,
  `TAHUN_ANGGARAN` int(11) DEFAULT NULL,
  `PAGU_ALAT` bigint(20) DEFAULT NULL,
  `TANGGAL` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pagu`
--

INSERT INTO `pagu` (`ID_PAGU`, `ID_JURUSAN`, `TAHUN_ANGGARAN`, `PAGU_ALAT`, `TANGGAL`) VALUES
(1, 1, 2016, 300000, '2016-10-26 00:00:00'),
(11, 1, 2016, 1100000, '2016-10-27 08:10:58'),
(12, 1, 2016, 30000, '2016-10-27 09:10:52');

-- --------------------------------------------------------

--
-- Table structure for table `paket`
--

CREATE TABLE `paket` (
  `ID_PAKET` int(11) NOT NULL,
  `ID_USER` int(11) DEFAULT NULL,
  `ID_TIM_HPS` int(11) DEFAULT NULL,
  `TAHUN_ANGGARAN` int(11) DEFAULT NULL,
  `NAMA_PAKET` text,
  `STATUS` decimal(8,0) DEFAULT NULL,
  `TANGGAL_DIBUAT` datetime DEFAULT NULL,
  `TOTAL_ANGGARAN` bigint(20) DEFAULT NULL,
  `LAST_UPDATE` datetime DEFAULT NULL,
  `KETERANGAN_GAGAL_LELANG` text,
  `PENYEDIA` varchar(50) DEFAULT NULL,
  `WAKTU_PENGADAAN` int(11) DEFAULT NULL,
  `TYPE_WAKTU` varchar(20) DEFAULT NULL,
  `STATUS_BAYAR` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `NIP` varchar(20) NOT NULL,
  `NAMA_PEGAWAI` varchar(50) DEFAULT NULL,
  `NO_HP` decimal(13,0) DEFAULT NULL,
  `EMAIL` char(50) DEFAULT NULL,
  `IS_TEKNISI` tinyint(1) DEFAULT NULL,
  `HPS_CERTIFIED` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`NIP`, `NAMA_PEGAWAI`, `NO_HP`, `EMAIL`, `IS_TEKNISI`, `HPS_CERTIFIED`) VALUES
('10', 'Cindy', '80', 'c@gmail.com', 0, 0),
('101010101010', 'Luthpi', '810101010', 'j@gmail.com', 0, 0),
('11', 'Tiara', '80', 't@gmail.com', 0, 0),
('11111111', 'Adit', '8111111', 'a@gmail.com', 1, 0),
('12', 'BobiT', '80', 'b@gmcil.com', 0, 0),
('13', 'Amos', '80', 'am@gmail.com', 0, 0),
('14', 'Amara', '2080', 'asd@asd', 0, 0),
('15', 'Amru', '1238', 'a@amc.', 0, 0),
('16', 'Amri', '12389', 'a@asma', 0, 0),
('17', 'Asma', '23123', 'as@ma', 0, 0),
('18', 'Amsah', '2912', 'a2S@asm', 0, 0),
('19', 'Amy', '2892839', 'asdm@ma', 0, 0),
('20', 'Darwin', '2938192', 'dar@gmail.com', 0, 0),
('2222222', 'Andi', '8222222', 'b@gmail.com', 0, 0),
('3333333', 'Andika', '83333333', 'c@gmail.com', 0, 0),
('4444444', 'Annisa', '84444444', 'd@gmail.com', 0, 0),
('555555', 'Arif', '8555555', 'e@gmail.com', 0, 0),
('6', 'Didi', '6345345', 'didi@gmail.com', 0, 1),
('66666', 'Budi', '8666666', 'f@gmail.com', 0, 0),
('7', 'Charlie', '239124', 'ch@gmail.com', 1, 0),
('7777777', 'Chandri', '87777777', 'g@gmail.com', 0, 0),
('8', 'Bima', '29312831', 'bim@gmail.com', 0, 1),
('8888888', 'Erwin', '8888888', 'h@gmail.com', 0, 0),
('9', 'Arjuna', '92315123', 'arj@gmail.com', 1, 0),
('99999999', 'Julid', '89999999', 'i@gmail.com', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pemenang`
--

CREATE TABLE `pemenang` (
  `ID_PEMENANG` int(11) NOT NULL,
  `ID_PAKET` int(11) DEFAULT NULL,
  `NAMA_PERUSAHAAN` varchar(100) DEFAULT NULL,
  `NPWP` varchar(25) DEFAULT NULL,
  `ALAMAT` text,
  `PIC_PERUSAHAAN` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `penerimaan`
--

CREATE TABLE `penerimaan` (
  `ID_PENERIMAAN` int(11) NOT NULL,
  `ID_PAKET` int(11) DEFAULT NULL,
  `ID_ALAT` int(11) DEFAULT NULL,
  `ID_TIM_PENERIMA` int(11) DEFAULT NULL,
  `TANGGAL_PENERIMAAN` datetime DEFAULT NULL,
  `JUMLAH` decimal(8,0) DEFAULT NULL,
  `KETERANGAN` text,
  `STATUS_KONFIRMASI` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `progress_paket`
--

CREATE TABLE `progress_paket` (
  `ID_PROGRESS_PAKET` int(11) NOT NULL,
  `ID_PAKET` int(11) DEFAULT NULL,
  `ID_USER` int(11) DEFAULT NULL,
  `ID_JENIS_USER` int(11) DEFAULT NULL,
  `ID_FASE` int(11) DEFAULT NULL,
  `ID_USULAN` int(11) DEFAULT NULL,
  `STATUS` decimal(8,0) DEFAULT NULL,
  `TANGGAL` datetime DEFAULT NULL,
  `REVISI_KE` decimal(8,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reminder`
--

CREATE TABLE `reminder` (
  `ID_REMINDER` int(11) NOT NULL,
  `ID_FASE` int(11) DEFAULT NULL,
  `ID_USER` int(11) DEFAULT NULL,
  `STATUS` decimal(8,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tahun_anggaran`
--

CREATE TABLE `tahun_anggaran` (
  `TAHUN_ANGGARAN` int(11) NOT NULL,
  `TANGGAL_MULAI` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tahun_anggaran`
--

INSERT INTO `tahun_anggaran` (`TAHUN_ANGGARAN`, `TANGGAL_MULAI`) VALUES
(2016, '2016-10-26');

-- --------------------------------------------------------

--
-- Table structure for table `tim_hps`
--

CREATE TABLE `tim_hps` (
  `ID_TIM_HPS` int(11) NOT NULL,
  `ID_USER` int(11) DEFAULT NULL,
  `NAMA_TIM` char(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tim_penerima`
--

CREATE TABLE `tim_penerima` (
  `ID_TIM_PENERIMA` int(11) NOT NULL,
  `ID_USER` int(11) DEFAULT NULL,
  `NAMA_TIM` char(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `ID_USER` int(11) NOT NULL,
  `ID_JENIS_USER` int(11) DEFAULT NULL,
  `ID_JURUSAN` int(11) DEFAULT NULL,
  `NIP` varchar(20) DEFAULT NULL,
  `USERNAME` varchar(50) DEFAULT NULL,
  `PASSWORD` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`ID_USER`, `ID_JENIS_USER`, `ID_JURUSAN`, `NIP`, `USERNAME`, `PASSWORD`) VALUES
(11, 1, 1, '11111111', 'TeknisiJTK', '5f4dcc3b5aa765d61d8327deb882cf99'),
(12, 2, 1, '2222222', 'KalabJTK', '5f4dcc3b5aa765d61d8327deb882cf99'),
(13, 3, 1, '3333333', 'KajurJTK', '7d6c31ed80bbe5992ae7dd22dfdadf70'),
(14, 3, 1, '10', 'Sekjur1JTK', '5f4dcc3b5aa765d61d8327deb882cf99'),
(15, 3, 1, '11', 'Sekjur2JTK', '5f4dcc3b5aa765d61d8327deb882cf99'),
(16, 1, 2, '7', 'TeknisiAN', '5f4dcc3b5aa765d61d8327deb882cf99'),
(17, 2, 2, '6', 'KalabAN', '5f4dcc3b5aa765d61d8327deb882cf99'),
(18, 3, 2, '8', 'KajurAN', '5f4dcc3b5aa765d61d8327deb882cf99'),
(19, 3, 2, '12', 'Sekjur1AN', '5f4dcc3b5aa765d61d8327deb882cf99'),
(20, 3, 2, '13', 'Sekjur2AN', '5f4dcc3b5aa765d61d8327deb882cf99'),
(21, 4, 0, '4444444', 'PD2', '5f4dcc3b5aa765d61d8327deb882cf99'),
(22, 5, 0, '555555', 'PPK', '5f4dcc3b5aa765d61d8327deb882cf99'),
(23, 6, 0, '66666', 'TimHPS', '5f4dcc3b5aa765d61d8327deb882cf99'),
(24, 7, 0, '7777777', 'ULP', '5f4dcc3b5aa765d61d8327deb882cf99'),
(25, 8, 0, '8888888', 'TimPenerima', '5f4dcc3b5aa765d61d8327deb882cf99'),
(26, 9, 0, '99999999', 'PPSPM', '5f4dcc3b5aa765d61d8327deb882cf99'),
(27, 10, 0, '101010101010', 'Direktur', '5f4dcc3b5aa765d61d8327deb882cf99'),
(28, 11, 0, '20', 'Adum', '5f4dcc3b5aa765d61d8327deb882cf99'),
(29, 99, 0, NULL, 'Admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `usulan`
--

CREATE TABLE `usulan` (
  `ID_USULAN` int(11) NOT NULL,
  `TAHUN_ANGGARAN` int(11) DEFAULT NULL,
  `ID_USER` int(11) DEFAULT NULL,
  `ID_JURUSAN` int(11) DEFAULT NULL,
  `NAMA_PAKET` text,
  `STATUS` decimal(8,0) DEFAULT NULL,
  `TANGGAL_DIBUAT` datetime DEFAULT NULL,
  `TOTAL_ANGGARAN` bigint(20) DEFAULT NULL,
  `LAST_UPDATE` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alat`
--
ALTER TABLE `alat`
  ADD PRIMARY KEY (`ID_ALAT`),
  ADD KEY `FK_RELATIONSHIP_10` (`ID_LOKASI`),
  ADD KEY `FK_RELATIONSHIP_16` (`ID_FASE`),
  ADD KEY `FK_RELATIONSHIP_18` (`ID_JURUSAN`),
  ADD KEY `FK_RELATIONSHIP_26` (`ID_PAKET`),
  ADD KEY `FK_RELATIONSHIP_29` (`ID_USULAN`),
  ADD KEY `FK_RELATIONSHIP_36` (`ID_KATEGORI`),
  ADD KEY `FK_RELATIONSHIP_6` (`ID_USER`);

--
-- Indexes for table `anggota_tim_hps`
--
ALTER TABLE `anggota_tim_hps`
  ADD PRIMARY KEY (`ID_TIM_HPS`,`NIP`),
  ADD KEY `FK_RELATIONSHIP_42` (`NIP`);

--
-- Indexes for table `anggota_tim_penerima`
--
ALTER TABLE `anggota_tim_penerima`
  ADD PRIMARY KEY (`ID_TIM_PENERIMA`,`NIP`),
  ADD KEY `FK_RELATIONSHIP_40` (`NIP`);

--
-- Indexes for table `bukti_pengadaan`
--
ALTER TABLE `bukti_pengadaan`
  ADD PRIMARY KEY (`ID_BUKTI`),
  ADD KEY `FK_RELATIONSHIP_35` (`ID_PAKET`);

--
-- Indexes for table `fase`
--
ALTER TABLE `fase`
  ADD PRIMARY KEY (`ID_FASE`);

--
-- Indexes for table `jenis_user`
--
ALTER TABLE `jenis_user`
  ADD PRIMARY KEY (`ID_JENIS_USER`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`ID_JURUSAN`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`ID_KATEGORI`);

--
-- Indexes for table `kontrak`
--
ALTER TABLE `kontrak`
  ADD PRIMARY KEY (`ID_KONTRAK`),
  ADD KEY `FK_RELATIONSHIP_24` (`ID_USER`),
  ADD KEY `FK_RELATIONSHIP_25` (`ID_PAKET`);

--
-- Indexes for table `lokasi`
--
ALTER TABLE `lokasi`
  ADD PRIMARY KEY (`ID_LOKASI`),
  ADD KEY `FK_RELATIONSHIP_15` (`ID_JURUSAN`);

--
-- Indexes for table `pagu`
--
ALTER TABLE `pagu`
  ADD PRIMARY KEY (`ID_PAGU`),
  ADD KEY `FK_RELATIONSHIP_17` (`ID_JURUSAN`),
  ADD KEY `FK_RELATIONSHIP_46` (`TAHUN_ANGGARAN`);

--
-- Indexes for table `paket`
--
ALTER TABLE `paket`
  ADD PRIMARY KEY (`ID_PAKET`),
  ADD KEY `FK_REFERENCE_40` (`TAHUN_ANGGARAN`),
  ADD KEY `FK_RELATIONSHIP_2` (`ID_TIM_HPS`),
  ADD KEY `FK_RELATIONSHIP_4` (`ID_USER`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`NIP`);

--
-- Indexes for table `pemenang`
--
ALTER TABLE `pemenang`
  ADD PRIMARY KEY (`ID_PEMENANG`),
  ADD KEY `FK_RELATIONSHIP_47` (`ID_PAKET`);

--
-- Indexes for table `penerimaan`
--
ALTER TABLE `penerimaan`
  ADD PRIMARY KEY (`ID_PENERIMAAN`),
  ADD KEY `FK_RELATIONSHIP_37` (`ID_PAKET`),
  ADD KEY `FK_RELATIONSHIP_8` (`ID_TIM_PENERIMA`),
  ADD KEY `FK_RELATIONSHIP_9` (`ID_ALAT`);

--
-- Indexes for table `progress_paket`
--
ALTER TABLE `progress_paket`
  ADD PRIMARY KEY (`ID_PROGRESS_PAKET`),
  ADD KEY `FK_RELATIONSHIP_12` (`ID_FASE`),
  ADD KEY `FK_RELATIONSHIP_13` (`ID_USER`),
  ADD KEY `FK_RELATIONSHIP_14` (`ID_PAKET`),
  ADD KEY `FK_RELATIONSHIP_28` (`ID_USULAN`),
  ADD KEY `FK_RELATIONSHIP_38` (`ID_JENIS_USER`);

--
-- Indexes for table `reminder`
--
ALTER TABLE `reminder`
  ADD PRIMARY KEY (`ID_REMINDER`),
  ADD KEY `FK_RELATIONSHIP_43` (`ID_FASE`),
  ADD KEY `FK_RELATIONSHIP_44` (`ID_USER`);

--
-- Indexes for table `tahun_anggaran`
--
ALTER TABLE `tahun_anggaran`
  ADD PRIMARY KEY (`TAHUN_ANGGARAN`);

--
-- Indexes for table `tim_hps`
--
ALTER TABLE `tim_hps`
  ADD PRIMARY KEY (`ID_TIM_HPS`),
  ADD KEY `FK_RELATIONSHIP_32` (`ID_USER`);

--
-- Indexes for table `tim_penerima`
--
ALTER TABLE `tim_penerima`
  ADD PRIMARY KEY (`ID_TIM_PENERIMA`),
  ADD KEY `FK_RELATIONSHIP_33` (`ID_USER`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID_USER`),
  ADD KEY `FK_RELATIONSHIP_1` (`ID_JENIS_USER`),
  ADD KEY `FK_RELATIONSHIP_23` (`ID_JURUSAN`),
  ADD KEY `FK_RELATIONSHIP_31` (`NIP`);

--
-- Indexes for table `usulan`
--
ALTER TABLE `usulan`
  ADD PRIMARY KEY (`ID_USULAN`),
  ADD KEY `FK_RELATIONSHIP_27` (`ID_USER`),
  ADD KEY `FK_RELATIONSHIP_30` (`ID_JURUSAN`),
  ADD KEY `FK_RELATIONSHIP_45` (`TAHUN_ANGGARAN`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alat`
--
ALTER TABLE `alat`
  MODIFY `ID_ALAT` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fase`
--
ALTER TABLE `fase`
  MODIFY `ID_FASE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `jenis_user`
--
ALTER TABLE `jenis_user`
  MODIFY `ID_JENIS_USER` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT for table `jurusan`
--
ALTER TABLE `jurusan`
  MODIFY `ID_JURUSAN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `ID_KATEGORI` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `kontrak`
--
ALTER TABLE `kontrak`
  MODIFY `ID_KONTRAK` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lokasi`
--
ALTER TABLE `lokasi`
  MODIFY `ID_LOKASI` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `pagu`
--
ALTER TABLE `pagu`
  MODIFY `ID_PAGU` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `paket`
--
ALTER TABLE `paket`
  MODIFY `ID_PAKET` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pemenang`
--
ALTER TABLE `pemenang`
  MODIFY `ID_PEMENANG` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `penerimaan`
--
ALTER TABLE `penerimaan`
  MODIFY `ID_PENERIMAAN` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `progress_paket`
--
ALTER TABLE `progress_paket`
  MODIFY `ID_PROGRESS_PAKET` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tim_hps`
--
ALTER TABLE `tim_hps`
  MODIFY `ID_TIM_HPS` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tim_penerima`
--
ALTER TABLE `tim_penerima`
  MODIFY `ID_TIM_PENERIMA` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `ID_USER` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `usulan`
--
ALTER TABLE `usulan`
  MODIFY `ID_USULAN` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `alat`
--
ALTER TABLE `alat`
  ADD CONSTRAINT `FK_RELATIONSHIP_10` FOREIGN KEY (`ID_LOKASI`) REFERENCES `lokasi` (`ID_LOKASI`),
  ADD CONSTRAINT `FK_RELATIONSHIP_16` FOREIGN KEY (`ID_FASE`) REFERENCES `fase` (`ID_FASE`),
  ADD CONSTRAINT `FK_RELATIONSHIP_18` FOREIGN KEY (`ID_JURUSAN`) REFERENCES `jurusan` (`ID_JURUSAN`),
  ADD CONSTRAINT `FK_RELATIONSHIP_26` FOREIGN KEY (`ID_PAKET`) REFERENCES `paket` (`ID_PAKET`),
  ADD CONSTRAINT `FK_RELATIONSHIP_29` FOREIGN KEY (`ID_USULAN`) REFERENCES `usulan` (`ID_USULAN`),
  ADD CONSTRAINT `FK_RELATIONSHIP_36` FOREIGN KEY (`ID_KATEGORI`) REFERENCES `kategori` (`ID_KATEGORI`),
  ADD CONSTRAINT `FK_RELATIONSHIP_6` FOREIGN KEY (`ID_USER`) REFERENCES `user` (`ID_USER`);

--
-- Constraints for table `anggota_tim_hps`
--
ALTER TABLE `anggota_tim_hps`
  ADD CONSTRAINT `FK_RELATIONSHIP_41` FOREIGN KEY (`ID_TIM_HPS`) REFERENCES `tim_hps` (`ID_TIM_HPS`),
  ADD CONSTRAINT `FK_RELATIONSHIP_42` FOREIGN KEY (`NIP`) REFERENCES `pegawai` (`NIP`);

--
-- Constraints for table `anggota_tim_penerima`
--
ALTER TABLE `anggota_tim_penerima`
  ADD CONSTRAINT `FK_RELATIONSHIP_39` FOREIGN KEY (`ID_TIM_PENERIMA`) REFERENCES `tim_penerima` (`ID_TIM_PENERIMA`),
  ADD CONSTRAINT `FK_RELATIONSHIP_40` FOREIGN KEY (`NIP`) REFERENCES `pegawai` (`NIP`);

--
-- Constraints for table `bukti_pengadaan`
--
ALTER TABLE `bukti_pengadaan`
  ADD CONSTRAINT `FK_RELATIONSHIP_35` FOREIGN KEY (`ID_PAKET`) REFERENCES `paket` (`ID_PAKET`);

--
-- Constraints for table `kontrak`
--
ALTER TABLE `kontrak`
  ADD CONSTRAINT `FK_RELATIONSHIP_24` FOREIGN KEY (`ID_USER`) REFERENCES `user` (`ID_USER`),
  ADD CONSTRAINT `FK_RELATIONSHIP_25` FOREIGN KEY (`ID_PAKET`) REFERENCES `paket` (`ID_PAKET`);

--
-- Constraints for table `lokasi`
--
ALTER TABLE `lokasi`
  ADD CONSTRAINT `FK_RELATIONSHIP_15` FOREIGN KEY (`ID_JURUSAN`) REFERENCES `jurusan` (`ID_JURUSAN`);

--
-- Constraints for table `pagu`
--
ALTER TABLE `pagu`
  ADD CONSTRAINT `FK_RELATIONSHIP_17` FOREIGN KEY (`ID_JURUSAN`) REFERENCES `jurusan` (`ID_JURUSAN`),
  ADD CONSTRAINT `FK_RELATIONSHIP_46` FOREIGN KEY (`TAHUN_ANGGARAN`) REFERENCES `tahun_anggaran` (`TAHUN_ANGGARAN`);

--
-- Constraints for table `paket`
--
ALTER TABLE `paket`
  ADD CONSTRAINT `FK_REFERENCE_40` FOREIGN KEY (`TAHUN_ANGGARAN`) REFERENCES `tahun_anggaran` (`TAHUN_ANGGARAN`),
  ADD CONSTRAINT `FK_RELATIONSHIP_2` FOREIGN KEY (`ID_TIM_HPS`) REFERENCES `tim_hps` (`ID_TIM_HPS`),
  ADD CONSTRAINT `FK_RELATIONSHIP_4` FOREIGN KEY (`ID_USER`) REFERENCES `user` (`ID_USER`);

--
-- Constraints for table `pemenang`
--
ALTER TABLE `pemenang`
  ADD CONSTRAINT `FK_RELATIONSHIP_47` FOREIGN KEY (`ID_PAKET`) REFERENCES `paket` (`ID_PAKET`);

--
-- Constraints for table `penerimaan`
--
ALTER TABLE `penerimaan`
  ADD CONSTRAINT `FK_RELATIONSHIP_37` FOREIGN KEY (`ID_PAKET`) REFERENCES `paket` (`ID_PAKET`),
  ADD CONSTRAINT `FK_RELATIONSHIP_8` FOREIGN KEY (`ID_TIM_PENERIMA`) REFERENCES `tim_penerima` (`ID_TIM_PENERIMA`),
  ADD CONSTRAINT `FK_RELATIONSHIP_9` FOREIGN KEY (`ID_ALAT`) REFERENCES `alat` (`ID_ALAT`);

--
-- Constraints for table `progress_paket`
--
ALTER TABLE `progress_paket`
  ADD CONSTRAINT `FK_RELATIONSHIP_12` FOREIGN KEY (`ID_FASE`) REFERENCES `fase` (`ID_FASE`),
  ADD CONSTRAINT `FK_RELATIONSHIP_13` FOREIGN KEY (`ID_USER`) REFERENCES `user` (`ID_USER`),
  ADD CONSTRAINT `FK_RELATIONSHIP_14` FOREIGN KEY (`ID_PAKET`) REFERENCES `paket` (`ID_PAKET`),
  ADD CONSTRAINT `FK_RELATIONSHIP_28` FOREIGN KEY (`ID_USULAN`) REFERENCES `usulan` (`ID_USULAN`),
  ADD CONSTRAINT `FK_RELATIONSHIP_38` FOREIGN KEY (`ID_JENIS_USER`) REFERENCES `jenis_user` (`ID_JENIS_USER`);

--
-- Constraints for table `reminder`
--
ALTER TABLE `reminder`
  ADD CONSTRAINT `FK_RELATIONSHIP_43` FOREIGN KEY (`ID_FASE`) REFERENCES `fase` (`ID_FASE`),
  ADD CONSTRAINT `FK_RELATIONSHIP_44` FOREIGN KEY (`ID_USER`) REFERENCES `user` (`ID_USER`);

--
-- Constraints for table `tim_hps`
--
ALTER TABLE `tim_hps`
  ADD CONSTRAINT `FK_RELATIONSHIP_32` FOREIGN KEY (`ID_USER`) REFERENCES `user` (`ID_USER`);

--
-- Constraints for table `tim_penerima`
--
ALTER TABLE `tim_penerima`
  ADD CONSTRAINT `FK_RELATIONSHIP_33` FOREIGN KEY (`ID_USER`) REFERENCES `user` (`ID_USER`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_RELATIONSHIP_1` FOREIGN KEY (`ID_JENIS_USER`) REFERENCES `jenis_user` (`ID_JENIS_USER`),
  ADD CONSTRAINT `FK_RELATIONSHIP_23` FOREIGN KEY (`ID_JURUSAN`) REFERENCES `jurusan` (`ID_JURUSAN`),
  ADD CONSTRAINT `FK_RELATIONSHIP_31` FOREIGN KEY (`NIP`) REFERENCES `pegawai` (`NIP`);

--
-- Constraints for table `usulan`
--
ALTER TABLE `usulan`
  ADD CONSTRAINT `FK_RELATIONSHIP_27` FOREIGN KEY (`ID_USER`) REFERENCES `user` (`ID_USER`),
  ADD CONSTRAINT `FK_RELATIONSHIP_30` FOREIGN KEY (`ID_JURUSAN`) REFERENCES `jurusan` (`ID_JURUSAN`),
  ADD CONSTRAINT `FK_RELATIONSHIP_45` FOREIGN KEY (`TAHUN_ANGGARAN`) REFERENCES `tahun_anggaran` (`TAHUN_ANGGARAN`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
